package errör

import (
	"fmt"
	"runtime"
)

const mustMsg = "an error occurred where it must not"

// Must takes any value and an error and returns the value, but panics if the
// error is not nil.
//
// Can be used to wrap ‘constructors’ like so many Must* functions.
func Must[T any](t T, err error) T {
	if err == nil {
		return t
	}

	if _, file, line, ok := runtime.Caller(1); ok {
		panic(fmt.Errorf("%s:%d: %s: %w", file, line, mustMsg, err))
	}

	panic(fmt.Errorf("%s:\n%w", mustMsg, err))
}
