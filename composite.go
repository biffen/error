package errör

import (
	"errors"
	"fmt"
)

var _ error = (CompositeError)(nil)

// Composite creates a composite error containing all the supplied errors.
//
// Nil errors are ignored.
//
// If no (non-nil) errors are supplied then the resulting error is nil.
//
// If only one (non-nil) error is supplied then it is returned unchanged.
func Composite(errors ...error) error {
	c := make([]error, 0, len(errors))

	for _, e := range errors {
		if e == nil {
			continue
		}

		if d, ok := e.(CompositeError); ok {
			c = append(c, d...)

			continue
		}

		c = append(c, e)
	}

	switch len(c) {
	case 0:
		return nil

	case 1:
		return c[0]

	default:
		return CompositeError(c)
	}
}

// CompositeError is a container of multiple errors.
type CompositeError []error

// As fulfils errors.As by applying it to each contained error, in order, until
// one matches.
func (e CompositeError) As(target interface{}) bool {
	for _, err := range e {
		if errors.As(err, target) {
			return true
		}
	}

	return false
}

// Error returns a string consisting of all the contained errors’ messages.
func (e CompositeError) Error() string {
	if e == nil {
		return ""
	}

	return fmt.Sprintf("composite error: %q", []error(e))
}

// Is fulfils errors.Is and returns true if any of the contained errors does.
func (e CompositeError) Is(target error) bool {
	for _, err := range e {
		if errors.Is(err, target) {
			return true
		}
	}

	return false
}

// Unwrap delegates the unwrapping to the contained errors in order. If
// unwrapping the first error doesn’t give a result, the next one is tried, and
// so on.
//
// If a contained error can be unwrapped but there are more contained errors, a
// new compound error is returned that can keep delegating the unwrapping. This
// way continuous unwrapping will ‘visit’ all the contained errors.
func (e CompositeError) Unwrap() error {
	switch len(e) {
	case 0:
		return nil

	case 1:
		return errors.Unwrap(e[0])

	default:
		first := e[0]

		first = errors.Unwrap(first)
		if first == nil {
			return e[1:]
		}

		return Composite(first, e[1:])
	}
}
