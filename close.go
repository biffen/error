package errör

import "io"

// CloseAll is a utility to close a number of io.Closers and return a single
// composite error of all errors returned when closing them all, if any.
//
// Nil closers are OK and are simply skipped.
func CloseAll(closers ...io.Closer) (err error) {
	errs := make([]error, 0, len(closers))

	for _, closer := range closers {
		if closer == nil {
			continue
		}

		errs = append(errs, closer.Close())
	}

	return Composite(errs...)
}
