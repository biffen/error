package errör_test

import (
	"fmt"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	errör "gitlab.com/biffen/error"
)

var _ io.Closer = (*Closer)(nil)

func TestCloseAll(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		Errors []bool
		Error  bool
	}{
		{[]bool{}, false},
		{[]bool{false, false}, false},
		{[]bool{false}, false},

		{[]bool{true}, true},
		{[]bool{true, false, true}, true},
		{[]bool{true, true}, true},
		{[]bool{false, true, false}, true},
	} {
		c := c

		t.Run(fmt.Sprintf("%v", c.Errors), func(t *testing.T) {
			t.Parallel()

			closers := make([]io.Closer, 0, len(c.Errors))

			for _, e := range c.Errors {
				closers = append(closers, &Closer{Closed: false, Error: e})
			}

			err := errör.CloseAll(closers...)

			if c.Error {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}

			for _, closer := range closers {
				c, ok := closer.(*Closer)
				require.True(t, ok)
				assert.True(t, c.Closed)
			}
		})
	}
}

type Closer struct {
	Closed, Error bool
}

func (c *Closer) Close() error {
	c.Closed = true

	if c.Error {
		return Error("")
	}

	return nil
}
