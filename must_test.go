package errör_test

import (
	"fmt"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	errör "gitlab.com/biffen/error"
)

func ExampleMust() {
	// Should `url.Parse` return an error then this code would panic.
	url := errör.Must(url.Parse("https://example.com"))

	fmt.Println(url)
	// Output:
	// https://example.com
}

func TestMust(t *testing.T) {
	t.Parallel()

	assert.Panics(t, func() {
		errör.Must(time.Parse("x", ""))
	})

	var u *url.URL

	assert.NotPanics(t, func() {
		u = errör.Must(url.Parse("http://example.com"))
	})

	assert.Equal(t, "http", u.Scheme)
}
