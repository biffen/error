package errör_test

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	errör "gitlab.com/biffen/error"
)

const (
	a = Error("a")
	b = Error("b")
	c = Error("c")
)

var _ error = Error("")

func ExampleError() {
	var (
		// An error.
		err1 = errors.New("first error")
		// A nil error.
		err2 error
		// Another error.
		err3 = errors.New("third error")
	)

	// Create a composite error from the three (the nil error will be
	// discarded).
	err := errör.Composite(err1, err2, err3)

	// Will print both errors’ messages combined.
	fmt.Println(err)

	// True since err contains err1.
	fmt.Println(errors.Is(err, err1))

	// Also true since err also contains err3.
	fmt.Println(errors.Is(err, err3))

	// Output:
	// composite error: ["first error" "third error"]
	// true
	// true
}

func TestCompositeError_Is(t *testing.T) {
	t.Parallel()

	assert.True(t, errors.Is(errör.Composite(os.ErrExist), os.ErrExist))
	assert.True(t, errors.Is(errör.Composite(os.ErrExist), os.ErrExist))
	assert.True(t, errors.Is(errör.Composite(a, os.ErrExist), os.ErrExist))
	assert.True(t, errors.Is(errör.Composite(os.ErrExist, a), os.ErrExist))
	assert.True(t, errors.Is(errör.Composite(a, os.ErrExist, b), os.ErrExist))

	assert.True(t, errors.Is(errör.Composite(
		a,
		fmt.Errorf("wrapped: %w", os.ErrExist),
		b,
	), os.ErrExist))
}

func TestCompositeError_Unwrap(t *testing.T) {
	t.Parallel()

	err := errör.Composite(
		a,
		fmt.Errorf("wrapped: %w", b),
		c,
	)
	assert.Error(t, err)
	assert.True(t, errors.Is(err, a))
	assert.Len(t, err, 3)

	err = errors.Unwrap(err)
	assert.Error(t, err)
	assert.False(t, errors.Is(err, a))
	assert.True(t, errors.Is(err, b))
	assert.Len(t, err, 2)

	err = errors.Unwrap(err)
	assert.Error(t, err)
	assert.True(t, errors.Is(err, b))
	assert.True(t, errors.Is(err, c))
	assert.Len(t, err, 2)

	err = errors.Unwrap(err)
	assert.Error(t, err)
	assert.False(t, errors.Is(err, b))
	assert.True(t, errors.Is(err, c))
	assert.Len(t, err, 1)

	err = errors.Unwrap(err)
	assert.NoError(t, err)
}

func TestError(t *testing.T) {
	t.Parallel()

	t.Run("none", func(t *testing.T) {
		t.Parallel()

		assert.NoError(t, errör.Composite())
		assert.NoError(t, errör.Composite(nil))
	})

	t.Run("single", func(t *testing.T) {
		t.Parallel()

		err := errör.Composite(a)
		assert.Error(t, err)
		assert.Equal(t, a, err)
		assert.Equal(t, "a", err.Error())
	})

	t.Run("two", func(t *testing.T) {
		t.Parallel()

		err := errör.Composite(a, b)
		assert.Error(t, err)
		assert.Contains(t, err.Error(), `["a" "b"]`)
	})

	t.Run("flatten", func(t *testing.T) {
		t.Parallel()

		err := errör.Composite(a, b)
		err = errör.Composite(err, c)

		assert.True(t, errors.Is(err, a))
		assert.True(t, errors.Is(err, b))
		assert.True(t, errors.Is(err, c))
		assert.Contains(t, err.Error(), `["a" "b" "c"]`)
	})
}

type Error string

func (e Error) Error() string {
	return string(e)
}
